package com.td.elevatehackathon.tdtorontoapi.service;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.td.elevatehackathon.tdtorontoapi.dto.LocationData;
import com.td.elevatehackathon.tdtorontoapi.dto.LocationDataResponse;

public interface PostService {

    LocationDataResponse getAddressesFromAddress (LocationData userLocation, int radius, ArrayList<String> arrayList) 
    throws JsonParseException, JsonMappingException, IOException;

}
