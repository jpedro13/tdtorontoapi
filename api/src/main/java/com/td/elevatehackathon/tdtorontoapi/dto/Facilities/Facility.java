package com.td.elevatehackathon.tdtorontoapi.dto.Facilities;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "FacilityID", "FacilityType", "FacilityName", "FacilityDisplayName" })
public class Facility {

	@JsonProperty("FacilityID")
	private String facilityID;
	@JsonProperty("FacilityType")
	private String facilityType;
	@JsonProperty("FacilityName")
	private String facilityName;
	@JsonProperty("FacilityDisplayName")
	private String facilityDisplayName;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("FacilityID")
	public String getFacilityID() {
		return facilityID;
	}

	@JsonProperty("FacilityID")
	public void setFacilityID(String facilityID) {
		this.facilityID = facilityID;
	}

	@JsonProperty("FacilityType")
	public String getFacilityType() {
		return facilityType;
	}

	@JsonProperty("FacilityType")
	public void setFacilityType(String facilityType) {
		this.facilityType = facilityType;
	}

	@JsonProperty("FacilityName")
	public String getFacilityName() {
		return facilityName;
	}

	@JsonProperty("FacilityName")
	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}

	@JsonProperty("FacilityDisplayName")
	public String getFacilityDisplayName() {
		return facilityDisplayName;
	}

	@JsonProperty("FacilityDisplayName")
	public void setFacilityDisplayName(String facilityDisplayName) {
		this.facilityDisplayName = facilityDisplayName;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}