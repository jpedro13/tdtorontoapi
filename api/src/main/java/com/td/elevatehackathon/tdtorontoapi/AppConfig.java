package com.td.elevatehackathon.tdtorontoapi;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.td.elevatehackathon.tdtorontoapi.service.PostService;
import com.td.elevatehackathon.tdtorontoapi.service.PostServiceImpl;

@Configuration
public class AppConfig {
	@Bean
	public PostService postService() {
		return new PostServiceImpl();
	}
}