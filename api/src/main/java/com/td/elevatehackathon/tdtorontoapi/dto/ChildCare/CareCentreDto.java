package com.td.elevatehackathon.tdtorontoapi.dto.ChildCare;

import java.util.ArrayList;

public class CareCentreDto {
	private double latitude;
	private double longitude;
	private String distance;
	private ArrayList<String> tags;
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public String getDistance() {
		return distance;
	}
	public void setDistance(String distance) {
		this.distance = distance;
	}
	public ArrayList<String> getTags() {
		return tags;
	}
	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
}
