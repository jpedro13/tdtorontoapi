package com.td.elevatehackathon.tdtorontoapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.td.elevatehackathon.tdtorontoapi.dto.ChildCare.CareCentreDto;

import com.td.elevatehackathon.tdtorontoapi.service.PostService;
import com.td.elevatehackathon.tdtorontoapi.dto.LocationData;
import com.td.elevatehackathon.tdtorontoapi.dto.LocationDataResponse;

import java.io.IOException;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.springframework.util.ResourceUtils;

@RestController
public class PostController {

    private final PostService service;

    public PostController (@Autowired PostService postService) {this.service = postService; }

    @RequestMapping(value = "/getCareCenter", method = RequestMethod.POST)
    public LocationDataResponse CareCenterGetRequest(@RequestBody CareCentreDto careCentreDto) throws JsonParseException, JsonMappingException, IOException {
        LocationData userLocation = new LocationData(careCentreDto.getLatitude(), careCentreDto.getLongitude());
        return service.getAddressesFromAddress(userLocation,  Integer.parseInt(careCentreDto.getDistance()), careCentreDto.getTags());
    }

    @RequestMapping(value = "/postStubCareCenter", method = RequestMethod.POST)
    public String stubCareCenterPostRequest(@RequestBody CareCentreDto careCentreDto) throws FileNotFoundException, IOException {

        InputStream is = PostController.class.getClassLoader().getResourceAsStream("classpath:Stub.json");
        BufferedReader buf = new BufferedReader(new InputStreamReader(is));
        
        String line = buf.readLine();
        StringBuilder sb = new StringBuilder();
                
        while(line != null){
           sb.append(line).append("\n");
           line = buf.readLine();
        }
                
        String fileAsString = sb.toString();
        return fileAsString;
    }

}