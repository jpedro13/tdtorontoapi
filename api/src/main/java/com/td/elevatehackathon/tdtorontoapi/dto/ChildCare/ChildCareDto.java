package com.td.elevatehackathon.tdtorontoapi.dto.ChildCare;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"LocationID",
"LocationName",
"AUSPICE",
"STR_NO",
"STREET",
"UNIT",
"PCODE",
"ward",
"PhoneNumber",
"bldg_type",
"bldgname",
"InfantSpace",
"ToddlerSpace",
"PreschoolSpace",
"KindergartenSpace",
"GradeOnePlusSpace",
"TotalSpace",
"SUBSIDY",
"GC_GEOID",
"lng",
"lat",
"run_date"
})
public class ChildCareDto {

@JsonProperty("LocationID")
private Integer locationID;
@JsonProperty("LocationName")
private String locationName;
@JsonProperty("AUSPICE")
private String aUSPICE;
@JsonProperty("STR_NO")
private String sTRNO;
@JsonProperty("STREET")
private String sTREET;
@JsonProperty("UNIT")
private String uNIT;
@JsonProperty("PCODE")
private String pCODE;
@JsonProperty("ward")
private Integer ward;
@JsonProperty("PhoneNumber")
private String phoneNumber;
@JsonProperty("bldg_type")
private String bldgType;
@JsonProperty("bldgname")
private String bldgname;
@JsonProperty("InfantSpace")
private Integer infantSpace;
@JsonProperty("ToddlerSpace")
private Integer toddlerSpace;
@JsonProperty("PreschoolSpace")
private Integer preschoolSpace;
@JsonProperty("KindergartenSpace")
private Integer kindergartenSpace;
@JsonProperty("GradeOnePlusSpace")
private Integer gradeOnePlusSpace;
@JsonProperty("TotalSpace")
private Integer totalSpace;
@JsonProperty("SUBSIDY")
private String sUBSIDY;
@JsonProperty("GC_GEOID")
private Integer gCGEOID;
@JsonProperty("lng")
private Double lng;
@JsonProperty("lat")
private Double lat;
@JsonProperty("run_date")
private String runDate;
@JsonIgnore
private Map<String, Object> additionalProperties = new HashMap<String, Object>();

@JsonProperty("LocationID")
public Integer getLocationID() {
return locationID;
}

@JsonProperty("LocationID")
public void setLocationID(Integer locationID) {
this.locationID = locationID;
}

@JsonProperty("LocationName")
public String getLocationName() {
return locationName;
}

@JsonProperty("LocationName")
public void setLocationName(String locationName) {
this.locationName = locationName;
}

@JsonProperty("AUSPICE")
public String getAUSPICE() {
return aUSPICE;
}

@JsonProperty("AUSPICE")
public void setAUSPICE(String aUSPICE) {
this.aUSPICE = aUSPICE;
}

@JsonProperty("STR_NO")
public String getSTRNO() {
return sTRNO;
}

@JsonProperty("STR_NO")
public void setSTRNO(String sTRNO) {
this.sTRNO = sTRNO;
}

@JsonProperty("STREET")
public String getSTREET() {
return sTREET;
}

@JsonProperty("STREET")
public void setSTREET(String sTREET) {
this.sTREET = sTREET;
}

@JsonProperty("UNIT")
public String getUNIT() {
return uNIT;
}

@JsonProperty("UNIT")
public void setUNIT(String uNIT) {
this.uNIT = uNIT;
}

@JsonProperty("PCODE")
public String getPCODE() {
return pCODE;
}

@JsonProperty("PCODE")
public void setPCODE(String pCODE) {
this.pCODE = pCODE;
}

@JsonProperty("ward")
public Integer getWard() {
return ward;
}

@JsonProperty("ward")
public void setWard(Integer ward) {
this.ward = ward;
}

@JsonProperty("PhoneNumber")
public String getPhoneNumber() {
return phoneNumber;
}

@JsonProperty("PhoneNumber")
public void setPhoneNumber(String phoneNumber) {
this.phoneNumber = phoneNumber;
}

@JsonProperty("bldg_type")
public String getBldgType() {
return bldgType;
}

@JsonProperty("bldg_type")
public void setBldgType(String bldgType) {
this.bldgType = bldgType;
}

@JsonProperty("bldgname")
public String getBldgname() {
return bldgname;
}

@JsonProperty("bldgname")
public void setBldgname(String bldgname) {
this.bldgname = bldgname;
}

@JsonProperty("InfantSpace")
public Integer getInfantSpace() {
return infantSpace;
}

@JsonProperty("InfantSpace")
public void setInfantSpace(Integer infantSpace) {
this.infantSpace = infantSpace;
}

@JsonProperty("ToddlerSpace")
public Integer getToddlerSpace() {
return toddlerSpace;
}

@JsonProperty("ToddlerSpace")
public void setToddlerSpace(Integer toddlerSpace) {
this.toddlerSpace = toddlerSpace;
}

@JsonProperty("PreschoolSpace")
public Integer getPreschoolSpace() {
return preschoolSpace;
}

@JsonProperty("PreschoolSpace")
public void setPreschoolSpace(Integer preschoolSpace) {
this.preschoolSpace = preschoolSpace;
}

@JsonProperty("KindergartenSpace")
public Integer getKindergartenSpace() {
return kindergartenSpace;
}

@JsonProperty("KindergartenSpace")
public void setKindergartenSpace(Integer kindergartenSpace) {
this.kindergartenSpace = kindergartenSpace;
}

@JsonProperty("GradeOnePlusSpace")
public Integer getGradeOnePlusSpace() {
return gradeOnePlusSpace;
}

@JsonProperty("GradeOnePlusSpace")
public void setGradeOnePlusSpace(Integer gradeOnePlusSpace) {
this.gradeOnePlusSpace = gradeOnePlusSpace;
}

@JsonProperty("TotalSpace")
public Integer getTotalSpace() {
return totalSpace;
}

@JsonProperty("TotalSpace")
public void setTotalSpace(Integer totalSpace) {
this.totalSpace = totalSpace;
}

@JsonProperty("SUBSIDY")
public String getSUBSIDY() {
return sUBSIDY;
}

@JsonProperty("SUBSIDY")
public void setSUBSIDY(String sUBSIDY) {
this.sUBSIDY = sUBSIDY;
}

@JsonProperty("GC_GEOID")
public Integer getGCGEOID() {
return gCGEOID;
}

@JsonProperty("GC_GEOID")
public void setGCGEOID(Integer gCGEOID) {
this.gCGEOID = gCGEOID;
}

@JsonProperty("lng")
public Double getLng() {
return lng;
}

@JsonProperty("lng")
public void setLng(Double lng) {
this.lng = lng;
}

@JsonProperty("lat")
public Double getLat() {
return lat;
}

@JsonProperty("lat")
public void setLat(Double lat) {
this.lat = lat;
}

@JsonProperty("run_date")
public String getRunDate() {
return runDate;
}

@JsonProperty("run_date")
public void setRunDate(String runDate) {
this.runDate = runDate;
}

@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return this.additionalProperties;
}

@JsonAnySetter
public void setAdditionalProperty(String name, Object value) {
this.additionalProperties.put(name, value);
}

}