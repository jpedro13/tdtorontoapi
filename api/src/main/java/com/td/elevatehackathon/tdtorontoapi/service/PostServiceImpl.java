package com.td.elevatehackathon.tdtorontoapi.service;

import java.util.*;

import java.io.FileNotFoundException;

import com.td.elevatehackathon.tdtorontoapi.dto.LocationData;
import com.td.elevatehackathon.tdtorontoapi.dto.LocationDataResponse;
import com.td.elevatehackathon.tdtorontoapi.dto.ChildCare.CareCentreDto;
import com.td.elevatehackathon.tdtorontoapi.dto.ChildCare.ChildCareDto;
import com.td.elevatehackathon.tdtorontoapi.dto.Facilities.FacilityDto;
import com.td.elevatehackathon.tdtorontoapi.dto.Facilities.Facility;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.net.URL;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

import org.springframework.util.ResourceUtils;

import org.json.*;

public class PostServiceImpl implements PostService {

	private final String googleBaseEndpoint = "https://maps.googleapis.com/maps/api/distancematrix/json?units=metric";
	private final String googleOrigin = "&origins=";
	private final String googleDest = "&destinations=";
	private final String googleAPIKey = "&key=AIzaSyAZIltzd9mqqxGN9l2kDYr8St3NzfgMt_8";

	public LocationDataResponse getAddressesFromAddress(LocationData userLocation, int radius, ArrayList<String> tags) {
		LocationDataResponse response = new LocationDataResponse();
		try {
			// Check what we're looking for in the tags
			if (tags.get(0).equals("childCare")) {
				response.setLocationsWithinRadius(fetchChildCare(userLocation, radius, tags));
			} else if (tags.get(0).equals("facilities")) {
				response.setLocationsWithinRadius(fetchFacilities(userLocation, radius, tags));
			} else if (tags.get(0).equals("childAndFamilyPrograms")) {

			} else if (tags.get(0).equals("Schools")) {

			} else if (tags.get(0).equals("Libraries")) {

			} else if (tags.get(0).equals("CommunityHubs")) {

			} else if (tags.get(0).equals("childrenYouthAndParenting")) {

			}

		} catch (Exception e) {
			System.out.println("Exception Hit: " + e);
			e.printStackTrace();
		}

		return response;
	}

	public List<LocationData> fetchChildCare(LocationData userLocation, int radius, ArrayList<String> tags)
			throws JsonParseException, JsonMappingException, IOException, JSONException {
		ObjectMapper mapper = new ObjectMapper();
		// ChildCareDto[] childCare =
		// mapper.readValue(ResourceUtils.getFile("classpath:child_care.json"),
		// ChildCareDto[].class);
		ChildCareDto[] childCare = mapper.readValue(
				PostServiceImpl.class.getClassLoader().getResourceAsStream("classpath:child_care.json"),
				ChildCareDto[].class);
		List<LocationData> list = new ArrayList<LocationData>();

		for (ChildCareDto place : childCare) {
			// Check Location

			double dist = checkRadius(userLocation.getLatitude(), userLocation.getLongitude(), place.getLat(),
					place.getLng(), radius);

			if (dist != -1) {
				List<String> info = new ArrayList<String>();
				boolean addFlag = false;
				if (tags.size() == 1) {
					// return all results within radius
					info.add("Infant Spaces: " + place.getInfantSpace());
					info.add("Toddler Spaces: " + place.getToddlerSpace());
					info.add("Preschool Spaces: " + place.getPreschoolSpace());
					info.add("Kindergarten Spaces: " + place.getKindergartenSpace());
					info.add("Grade 1 and Above Spaces: " + place.getGradeOnePlusSpace());
					list.add(new LocationData(place.getLocationName(), "N/A", place.getLat(), place.getLng(),
							(place.getSTRNO() + " " + place.getSTREET() + " " + place.getUNIT() + " " + place.getPCODE()),
							place.getPhoneNumber(), dist, info));
				} else {
					// Check that the location applies to the tags
					for (String tag : tags) {
						if (tag.equals("Infant") && place.getInfantSpace() > 0) {
							addFlag = true;
							info.add("Infant Spaces: " + place.getInfantSpace());
						}
						if (tag.equals("Toddler") && place.getToddlerSpace() > 0) {
							addFlag = true;
							info.add("Toddler Spaces: " + place.getToddlerSpace());
						}
						if (tag.equals("Preschool") && place.getPreschoolSpace() > 0) {
							addFlag = true;
							info.add("Preschool Spaces: " + place.getPreschoolSpace());
						}
						if (tag.equals("Kindergarten") && place.getKindergartenSpace() > 0) {
							addFlag = true;
							info.add("Kindergarten Spaces: " + place.getKindergartenSpace());
						}
						if (tag.equals("Grade 1 and Above") && place.getGradeOnePlusSpace() > 0) {
							addFlag = true;
							info.add("Grade 1 and Above Spaces: " + place.getGradeOnePlusSpace());
						}
					}
					if (addFlag) {
						addFlag = false; // this may be unecessary.
						list.add(new LocationData(place.getLocationName(), "N/A", place.getLat(), place.getLng(),
								(place.getSTRNO() + " " + place.getSTREET() + " " + place.getUNIT() + " " + place.getPCODE()),
								place.getPhoneNumber(), dist, info));
					}
				}
			}
		}
		return list;
	}

	public List<LocationData> fetchFacilities(LocationData userLocation, int radius, ArrayList<String> tags)
			throws JsonParseException, JsonMappingException, FileNotFoundException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		FacilityDto[] facilitiesJson = mapper.readValue(ResourceUtils.getFile("classpath:facilities.json"),
				FacilityDto[].class);
		List<LocationData> list = new ArrayList<LocationData>();

		for (FacilityDto facility : facilitiesJson) {
			float dist = checkRadius(userLocation.getLatitude(), userLocation.getLongitude(), facility.getAddress(),
					facility.getPostalCode(), radius);

			if (dist != (-1)) {
				List<String> info = new ArrayList<String>();
				if (tags.size() == 1) {
					// return all results within radius
					for (Facility f : facility.getFacilities().getFacility()) {
						info.add("Facility Availible: " + f.getFacilityDisplayName());
					}
					// info.add("Facilities Availible: " +
					// facility.getFacilities().getFacility());
					list.add(new LocationData(facility.getLocationName(), "N/A", 0.0, 0.0,
							(facility.getAddress() + facility.getPostalCode()), facility.getPhoneNumber(), dist, info));
					break;
				}
				// Check each facility inside the selected Facility
				boolean addFlag = false;
				for (Facility f : facility.getFacilities().getFacility()) {
					// Check that the location applies to the tags
					for (String tag : tags) {
						if (tag.equals("Water") && f.getFacilityType().equals("Water")) {
							addFlag = true;
							info.add("Facility Availible " + f.getFacilityDisplayName());
						}
						if (tag.equals("Sports") && f.getFacilityType().equals("Sports")) {
							addFlag = true;
							info.add("Facility Availible " + f.getFacilityDisplayName());
						}
						if (tag.equals("Playgrounds") && f.getFacilityType().equals("Playgrounds")) {
							addFlag = true;
							info.add("Facility Availible " + f.getFacilityDisplayName());
						}
						if (tag.equals("Others") && f.getFacilityType().equals("Others")) {
							addFlag = true;
							info.add("Facility Availible " + f.getFacilityDisplayName());
						}
					}
				}

				if (addFlag) {
					addFlag = false;
					list.add(new LocationData(facility.getLocationName(), "N/A", 0.0, 0.0,
							(facility.getAddress() + facility.getPostalCode()), facility.getPhoneNumber(), dist, info));
				}
			}
		}

		return list;
	}

	public double checkRadius(double originLat, double originLong, double destLat, double destLong, int radius)
			throws MalformedURLException, IOException {
		// Create the google distance url
		String finalCall = googleBaseEndpoint + googleOrigin + Double.toString(originLat) + ","
				+ Double.toString(originLong) + googleDest + Double.toString(destLat) + "%2C"
				+ Double.toString(destLong) + "%7C" + googleAPIKey;

		double dist = 0.0;

		// Create the call
		URL url = new URL(finalCall);

		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("Content-Type", "application/json");

		// Make the call
		int status = con.getResponseCode();

		// Read the call
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer content = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			content.append(inputLine);
		}
		in.close();

		// Read the dist from the json
		JSONObject json = new JSONObject(content.toString());
		dist = Double.parseDouble(json.getJSONArray("rows").getJSONObject(0).getJSONArray("elements").getJSONObject(0)
				.getJSONObject("distance").get("text").toString().split(" ")[0]);

		if (dist > radius) {
			return -1;
		}

		return dist;
	}

	public float checkRadius(double originLat, double originLong, String address, String postalCode, int radius) {
		float dist = (float) 0.0;
		return dist;
	}

}