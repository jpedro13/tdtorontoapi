package com.td.elevatehackathon.tdtorontoapi.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({ "Name", "Latitude", "Longitude" })
public class LocationData {

    @JsonProperty("Name")
	private String name;

	@JsonProperty("URL")
	private String url;

	@JsonProperty("Latitude")
	private double latitude;

	@JsonProperty("Longitude")
	private double longitude;

	@JsonProperty("Address")
	private String address;

	@JsonProperty("Phone Number")
	private String phoneNum;
	
	@JsonProperty("Distance")
	private double distance;

	@JsonProperty("Info")
	private List<String> info;

    public LocationData (double lat, double lon) {
        this.latitude = lat;
        this.longitude = lon;
    }

	public LocationData(String name, String url, double lat, double lon, String addr, String phone, double dist, List<String> info) {
		this.name = name;
		this.url = url;
		this.latitude = lat;
		this.longitude = lon;
		this.address = addr;
		this.phoneNum = phone;
		this.distance = dist;
		this.info = info;
	}

    // Getters
    public String getName() {
		return this.name;
	}

	public String getUrl() {
		return this.url;
	}

	public String getAddress() {
		return this.address;
	}

	public double getLatitude() {
		return this.latitude;
	}

	public double getLongitude() {
		return this.longitude;
	}

	public String getPhoneNum() {
		return this.phoneNum;
	}

	public double getDistance() {
		return this.distance;
	}
	
	public List<String> getInfo() {
		return info;
	}

   // Setters
	public void setName(String name) {
		this.name = name;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setLatitude(double lat) {
		this.latitude = lat;
	}

	public void setLongitude(double lon) {
		this.longitude = lon;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}
	
	public void setInfo(List<String> info) {
		this.info = info;
	}
}
