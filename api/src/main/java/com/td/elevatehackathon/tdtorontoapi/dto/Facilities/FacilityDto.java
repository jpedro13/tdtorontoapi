package com.td.elevatehackathon.tdtorontoapi.dto.Facilities;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({ "LocationID", "LocationName", "Address", "PostalCode", "Facilities", "Intersections",
		"PhoneNumber" })
public class FacilityDto {

	@JsonProperty("LocationID")
	private String locationID;
	@JsonProperty("LocationName")
	private String locationName;
	@JsonProperty("Address")
	private String address;
	@JsonProperty("PostalCode")
	private String postalCode;
	@JsonProperty("Facilities")
	private Facilities facilities;
	@JsonProperty("Intersections")
	private Intersections intersections;
	@JsonProperty("PhoneNumber")
	private String phoneNumber;
	@JsonIgnore
	private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonProperty("LocationID")
	public String getLocationID() {
		return locationID;
	}

	@JsonProperty("LocationID")
	public void setLocationID(String locationID) {
		this.locationID = locationID;
	}

	@JsonProperty("LocationName")
	public String getLocationName() {
		return locationName;
	}

	@JsonProperty("LocationName")
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	@JsonProperty("Address")
	public String getAddress() {
		return address;
	}

	@JsonProperty("Address")
	public void setAddress(String address) {
		this.address = address;
	}

	@JsonProperty("PostalCode")
	public String getPostalCode() {
		return postalCode;
	}

	@JsonProperty("PostalCode")
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	@JsonProperty("Facilities")
	public Facilities getFacilities() {
		return facilities;
	}

	@JsonProperty("Facilities")
	public void setFacilities(Facilities facilities) {
		this.facilities = facilities;
	}

	@JsonProperty("Intersections")
	public Intersections getIntersections() {
		return intersections;
	}

	@JsonProperty("Intersections")
	public void setIntersections(Intersections intersections) {
		this.intersections = intersections;
	}

	@JsonProperty("PhoneNumber")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	@JsonProperty("PhoneNumber")
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@JsonAnyGetter
	public Map<String, Object> getAdditionalProperties() {
		return this.additionalProperties;
	}

	@JsonAnySetter
	public void setAdditionalProperty(String name, Object value) {
		this.additionalProperties.put(name, value);
	}

}