package com.td.elevatehackathon.tdtorontoapi.dto;

import java.util.List;

public class LocationDataResponse {
    private List<LocationData> locationsWithinRadius;

    public List<LocationData> getLocationsWithinRadius () {
        return this.locationsWithinRadius;
    }

    public void setLocationsWithinRadius (List<LocationData> locations) {
        this.locationsWithinRadius = locations;
    }
}