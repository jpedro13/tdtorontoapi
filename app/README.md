# Instructions
## Running the app locally
### Install programs

install [git bash][git bash]  
install [node][node]

install angular-cli:  
```npm install -g @angular/cli``` 

install typings:  
```npm install -g typings```

install typescript:  
```npm install -g typescript``` 

### Clone Repo
in git bash (or other terminal):  
```git clone https://jpedro13@bitbucket.org/jpedro13/tdtorontoapi.git``` 

### Install Dependencies
navigate to tdtorontoapi/app folder  

install dependencies:  
```npm install``` 

### Run the app locally
run the app:  
```npm start```  

Navigate to http://localhost:4200/.  
The app will automatically reload if you change any of the source files.  

[git bash]: https://git-scm.com/downloads  
[node]: https://nodejs.org/en/  

