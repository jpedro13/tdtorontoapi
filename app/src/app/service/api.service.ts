import { HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/Rx';


@Injectable()
export class ApiService {
    base: string;

    constructor(private http: Http) { }

    // public get(url: string): any {
    //     this.http.get(url).pipe(map((response: any) => response.json()));
    // }

    public post(url: string, payload: any) {
        return this.http.post(url, payload, { headers: this.getHeaders() });
    }

    private getHeaders(): Headers {
        const headers = new Headers();

        headers.append('Content-Type', 'application/json');
        // headers.append('Authorization', 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJDQlAiLCJ0ZWFtX2lkIjoiMjgxMzc3MiIsImV4cCI6OTIyMzM3MjAzNjg1NDc3NSwiYXBwX2lkIjoiNWJiMzFhMDUtNmY3MC00NjNkLWJlMTctYjk2YzhhNjk3NjI5In0.wfXq75stYG7Hr3aAtIrplbQo6fyf8Kw8Xxp0196jY5g');
        return headers;
    }

}
