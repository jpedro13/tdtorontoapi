import { LocationRequest } from './interfaces/location-request';
/// <reference types="@types/googlemaps" />
import { Component, ViewChild } from '@angular/core';
import { } from '@types/googlemaps';
import { ApiService } from './service/api.service';
import { Endpoints } from './common/endpoints';
import { NgxSmartModalService } from 'ngx-smart-modal';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    @ViewChild('gmap') gmapElement: any;
    map: google.maps.Map;
    public showMap: boolean = false;
    public location: string = 'here';
    public placeClicked: string;
    public latitude: number;
    public longitude: number;
    public selectedDistance: number = 0;
    public tags: any = [];
    public primaryTag: string = null;
    public tagsArray = {};
    public locationSet: boolean = true;
    public distanceSet: boolean = false;
    public thisLat = 43.646827;
    public thisLong = -79.381217;
    public showStartOver: boolean = false;
    public center = new google.maps.LatLng(this.thisLat, this.thisLong);
    public markers = [];
    public pois: any = [];
    public clickedPoi: any = {};
    public infoKeys: any = null;

    constructor(private apiService: ApiService,
        public ngxSmartModalService: NgxSmartModalService) { }

    ngOnInit() {
        this.tagsArray = {
            childCare: ['Infant', 'Toddler', 'Preschool', 'Kindergarden', 'Grade 1 and Above'],
            facilities: ['Water', 'Sports', 'Playgrounds', 'Other'],
            childAndFamilyPrograms: [],
            schools: ['Elementary', 'High School', 'College', 'University', 'English', 'French'],
            libraries: [],
            communityHubs: [],
            childrenYouthAndParenting: ['Children Programs', 'Pregnancy and Parenting', 'Youth']
        }
        this.latitude = this.thisLat;
        this.longitude = this.thisLong;
    }


    ngAfterContentInit() {
        let mapProp = {
            center: this.center,
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
        let marker = new google.maps.Marker;
        marker.setPosition(this.center);
        marker.setAnimation(google.maps.Animation.BOUNCE);
        this.markers.push(marker);
        this.markers[0].setMap(this.map);
        google.maps.event.addListener(this.map, 'click', (event) => {
            if (!this.showStartOver) {
                this.selectPlaceOnMap(event);
                this.setLocationTrue();
            }
        });
    }


    public clickMap() {
        if (this.placeClicked !== 'clickMap') {
            this.placeClicked = 'clickMap';
            this.gmapElement.nativeElement.hidden = false;
            this.showMap = true;
            // TODO: Display selected address (aprox)
        }

    }

    public clickHere() {
        if (this.placeClicked !== 'clickHere') {
            this.placeClicked = 'clickHere';
            this.gmapElement.nativeElement.hidden = true;
            this.showMap = false;
            this.locationSet = true;
            this.latitude = this.thisLat;
            this.longitude = this.thisLong;
        }
    }

    public selectPlaceOnMap(event) {
        this.placeClicked = event.latLng.lat() + ',' + event.latLng.lng();
        this.latitude = event.latLng.lat();
        this.longitude = event.latLng.lng();
        this.gmapElement.nativeElement.hidden = true;
        this.locationSet = true;
        this.showMap = false;
        this.setLocationTrue();
        // console.log('this.placeClicked');
        // console.log(this.placeClicked);
    }

    public setLocationTrue() {
        this.locationSet = true;
    }

    public setDistance() {
        // TODO: Make distance dropdown only show up position is selected
        this.distanceSet = true;
    }

    public setPrimaryTag(tag) {
        this.primaryTag = tag;
        this.tags = [tag];
        console.log(this.tags);
    }

    public addSecondaryTag(secondaryTag: string) {
        if (this.tags.indexOf(secondaryTag) !== -1) {
            // tags array contains the secondary tag
            this.tags.splice(this.tags.indexOf(secondaryTag), 1);
        } else {
            // tags array doesnt containt the secondary tag
            this.tags.push(secondaryTag);
        }
        console.log('addSecondaryTag');
        console.log(this.tags);
    }

    public isSecondaryTagPresent(secondaryTag) {
        if (this.tags.indexOf(secondaryTag) !== -1) {
            return true;
        }
        else {
            return false;
        }
    }

    public resetForm() {
        this.location = 'here';
        this.placeClicked = null;
        // this.locationSet = false;
        this.distanceSet = false;
        this.tags = [];
        this.showStartOver = false;
        this.gmapElement.nativeElement.hidden = true;
        this.selectedDistance = 0;
        this.latitude = this.thisLat;
        this.longitude = this.thisLong;
        this.primaryTag = null;
        this.clearMarkers();
        let marker = new google.maps.Marker;
        marker.setAnimation(google.maps.Animation.BOUNCE);
        marker.setPosition(this.center);
        this.markers.push(marker);
        this.markers[0].setMap(this.map);
        this.pois = [];
        this.map.setCenter(this.center);
        this.map.setZoom(13);
        this.map.setMapTypeId(google.maps.MapTypeId.ROADMAP);
        console.log('this.tags: reset');
        console.log(this.tags);
    }

    public clearMarkers() {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(null);
        }
        this.markers = [];
    }

    public searchPlaces() {
        let locationRequest: LocationRequest = {
            latitude: this.latitude,
            longitude: this.longitude,
            distance: this.selectedDistance,
            tags: this.tags
        };
        console.log('locationRequest:');
        console.log(locationRequest);
        // Make API call

        // Stub markers
        this.showMap = true;
        this.showStartOver = true;
        this.gmapElement.nativeElement.hidden = false;
        var newCenter = new google.maps.LatLng(this.latitude, this.longitude);
        this.clearMarkers();
        let marker = new google.maps.Marker;
        marker.setPosition(newCenter);
        marker.setAnimation(google.maps.Animation.DROP);
        this.map.setCenter(newCenter);
        this.markers.push(marker);

        //TODO: Uncomment this to make post api call
        this.apiService.post(Endpoints.STUB_DATA(), locationRequest).subscribe(res => {
            this.pois = JSON.parse(res["_body"]).locationsWithinRadius;
            console.log('this.pois');
            console.log(this.pois);
            this.pois.forEach((element, index) => {
                var marker = new google.maps.Marker({
                    position: { lat: element.Latitude, lng: element.Longitude },
                    label: index.toString(),
                    map: this.map,
                    animation: google.maps.Animation.DROP
                });
                google.maps.event.addListener(marker, 'click', () => {
                    this.displayMarkerMessage(marker.getLabel());
                });
                this.markers.push(marker);
            });screenLeft
        },
            err => {
                console.log(err);
            })

        // TODO: Comment from here
        // this.pois = [
        //     {
        //         name: 'Child Care 1',
        //         url: 'www.childcareone.com',
        //         latitude: 43.660580,
        //         longitude: -79.382696,
        //         address: '123 happy lane, a2c4e5',
        //         phone: '123-456-7890',
        //         distance: 1.3,
        //         type: 'infant',
        //         info: {
        //             'Age Group': 'Infant',
        //             'Language': 'English',
        //             'Hours': '9am to 5pm',

        //         }
        //     },
        //     {
        //         name: 'The Best Pool',
        //         url: 'www.besthpool.com',
        //         latitude: 43.660518,
        //         longitude: -79.385689,
        //         address: '123 swimmers road, b2c4e5',
        //         phone: '123-456-7890',
        //         distance: 0.8,
        //         type: 'pool',
        //         info: {
        //             'Swimming lessons': 'Yes',
        //             'Price': '$3.75',
        //             'Hours': '10am to 8pm'
        //         }
        //     },
        //     {
        //         name: 'Sewing for kids',
        //         url: 'www.teachingkids.com',
        //         latitude: 43.661395,
        //         longitude: -79.386301,
        //         address: '123 knotting cul de sac, b4c4e5',
        //         phone: '123-456-7890',
        //         distance: 1.8,
        //         type: 'childCourse',
        //         info: {
        //             'Age Group': '3 to 10',
        //             'Price': '$13',
        //             'Hours': '10am to 8pm'
        //         }
        //     }
        // ];

        // TODO: to here





        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(this.map);
        }
        switch (this.selectedDistance) {
            case 1:
                this.map.setZoom(11);
                break;
            case 2:
                this.map.setZoom(12);
                break;
            case 3:
                this.map.setZoom(13);
                break;

            default:
                break;
        }

    }

    public displayMarkerMessage(markerLabel) {
        this.setModalData(markerLabel).then(res => {
            setTimeout((this.ngxSmartModalService.getModal('myModal').open(), 2000));
        });
    }

    public setModalData(markerLabel) {
        return new Promise(resolve => {
            this.clickedPoi = null;
            this.infoKeys = null;
            this.clickedPoi = this.pois[markerLabel];
            // if (this.clickedPoi.Info.length !== {}) {
            //     this.infoKeys = Object.keys(this.clickedPoi.Info)
            // }
            resolve(markerLabel);
        });
    }
}
