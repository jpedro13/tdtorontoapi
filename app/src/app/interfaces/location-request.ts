export interface LocationRequest {
    latitude: number;
    longitude: number;
    distance: number;
    tags: any;
}
