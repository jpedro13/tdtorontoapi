import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ApiService } from './service/api.service';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        HttpModule,
        NgxSmartModalModule.forRoot()
    ],
    providers: [ApiService],
    bootstrap: [AppComponent]
})
export class AppModule { }
